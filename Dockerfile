# fetch image base from docker hub
FROM python

# copy script to container path
ADD SpaceTracker.py /

# install pip modules
RUN pip install boto3
RUN pip install influxdb
RUN pip install humanize
RUN pip install argparse
RUN pip install elasticsearch 

# run the python script 
ENTRYPOINT [ "python", "./SpaceTracker.py" ]

