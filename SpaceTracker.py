#!/usr/bin/env python

import boto3 
import argparse
import humanize 
import json
from influxdb import InfluxDBClient
from elasticsearch import Elasticsearch
import time

''' This function taks ans parses arguments from the user '''
def create_args():
     
     # construct new argument parser 
     parser = argparse.ArgumentParser()

     # add arguments to parser instance 
     parser.add_argument('-e', '--endpoint-url', help="endpoint url for s3 object storage", required=True)
     parser.add_argument('-a', '--access-key', help='access key for s3 object storage', required=True)
     parser.add_argument('-s', '--secret-key', help='secret key for s3 object storage', required=True)
     parser.add_argument('-b', '--bucket-name', help='s3 bucket name', required=True)
     parser.add_argument('-i', '--influx-url', help='influxdb endpoint url', required=False)
     parser.add_argument('-u', '--elastic-url', help='elastic search endpoint url', required=False)

     # parse arguments 
     args = parser.parse_args()
     
     return args 

''' This function takes arguments and returns used size from wanted bucket by HTTP HEAD '''
def get_s3_stats(args):

    s3 = boto3.client('s3', endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key, aws_secret_access_key=args.secret_key)
    return s3.head_bucket(Bucket=args.bucket_name)['ResponseMetadata']['HTTPHeaders']['x-rgw-bytes-used']

''' This function converts the bytes returned size nicely '''
def convert_nicely(used_size):
    return humanize.naturalsize(used_size, binary=True)

''' This function build the json that will be written to influx '''
def build_json(args, used_size_nicely):
    
    # initialize empty json in case user havent chosen enything
    json_body = ""

    # in case the user asks for influx 
    if args.influx_url is not None and args.elastic_url is None:
        json_body = [
                {
                   "measurement": "s3_consumption",
                   "fields": {
                      "used_size": str(used_size_nicely.replace(" ", ""))
                      }
                }
            ]

    # in case the user asks for elastic 
    elif args.elastic_url is not None and args.influx_url is None:
        json_body = json.dumps({"used_size": used_size_nicely, "timestamp": round(time.time() * 1000)})

    return json_body

''' This function writes data to influxdb '''
def write_to_influx(args, json):
    
    # set variables 
    user = 'root'
    password = 'root'
    dbname = 's3'
    port = 8086
    
    # construct new influx client instance 
    client = InfluxDBClient(arguments.influx_url, port, user, password, dbname)

    # create new influx database 
    client.create_database(dbname)

    # write data to influx db 
    client.write_points(json)

''' This function writes data to elastic search '''
def write_to_elastic(args, json):

    # construct new elastic search instance 
    elastic = Elasticsearch(args.elastic_url)

    # elastic search index 
    es_index = 's3'

    # build index mapping json 
    mapping_json = create_mappings() 

    # creates an index in case it doesnt exist 
    if not elastic.indices.exists(es_index):
        elastic.indices.create(index=es_index, body=mapping_json)

    elastic.index(index=es_index, doc_type='doc', body=json)

''' This function creates mappings to convert fields to visualized values '''
def create_mappings():
    mapping = '''
        {
            "mappings": {
            "doc": {
                "properties": {
                    "timestamp": {
                    "type": "date"
                        }
                    }
                }
            }
        }'''

    return mapping 

if __name__ == "__main__":

    # get arguments from user 
    arguments = create_args()

    # get s3 used size stats 
    used_bytes = get_s3_stats(arguments)
   
    # get s3 used size nicely converted 
    used_size_nicely = convert_nicely(used_bytes)
    
    # build json according to user's choise 
    json = build_json(arguments, used_size_nicely)

    # if user wants to push to influx, then write json
    if arguments.influx_url is not None: 
        write_to_influx(arguments, json)

    elif arguments.elastic_url is not None:
        write_to_elastic(arguments, json)

    # print formatted size to the user if none of the above was chosen 
    else:
        print("used size: {}".format(used_size_nicely))
